import pandas as pd

csv_path='https://s3-api.us-geo.objectstorage.softlayer.net/cf-courses-data/CognitiveClass/PY0101EN/labs/top_selling_albums.csv'
df = pd.read_csv(csv_path)
# print(df.head())

xlsx_path='https://ibm.box.com/shared/static/mzd4exo31la6m7neva2w45dstxfg5s86.xlsx'

dfx = pd.read_excel(xlsx_path)
# print(dfx.head())

x = dfx['Length']
# print(x)

y = dfx[['Length', 'Artist']]
# print(y)

z1 = dfx.iloc[0, 0]
z2 = dfx.iloc[1, 0]
# print(z2)
z3 = dfx.loc[0,'Artist']
print(z3)

z4 = dfx.loc[0,'Released']
print(z4)

print('name: {}, year: {}'.format(dfx.loc[0,'Artist'], dfx.loc[0,'Released']))


