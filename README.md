# Kiến thức cơ bản cần có khi học deep learning

### 1. Phân biệt scalars, vectors, ma trận, và tensors?

### 1.1 Scalar

Scalar hay còn gọi là vô hướng hay là một điểm trong hình học. Nó biểu diễn một số trong đại số tuyển tính và khác với hầu hết các đại lượng khác trong đại số tuyển tính (thường là một mảng nhiều số hay vector).

### 1.2 Vector

Một vector có thể coi là một tập hợp có quan tâm đến thứ tự (ordered numbers) và thường được biểu diễn bằng một chữ in thường. Ví dụ vector x = [1, 2] trong đó x1 = 1 là phần tử đầu tiên của vector x. x2 = 2 là phần tử thứ hai của vector x.

### 1.3 Matrix

Tiếng việt còn gọi là ma trận là một tập hợp các vecto có cùng kiểu và số chiều và được biểu diễn bởi một mảng hai chiều. Chúng ta có thể tưởng tượng nó giống như một bảng dữ liệu có tên trường và các giá trị của chúng tương ứng với từng bản ghi trong cơ sơ dữ liệu. Trong đó, một đối tượng được biểu diễn dưới dạng một hàng (row) trong ma trận và một đặc trưng được biểu diễn dưới dạng cột (column). Tại mỗi đối tượng có một giá trị số. Tên của một biến chữ hoa thường được đặt cho ma trận in đậm, chẳng hạn như AA.

### 1.4 Tensor

Tensor là một khái niệm xuất hiện khá nhiều khi học Machine Learning đặc biệt là lĩnh vực Deep Learning. Nó thực chất có thể hiểu là một đối tượng tương tự như ma trận nhưng với số chiều lớn hơn 2. Nói chung, các phần tử trong một mảng được phân phối trong một không gian có tọa độ nhiều chiều, mà chúng ta gọi là một tensor. Framework khá phổ biến chúng ta hay sử dụng đó là Tensorflow tức là tính toán dựa trên các Tensor.
Chúng ta sử dụng A để kí hiệu cho tenor A. Phần tử có tọa độ (i, j, k) trong tensor A được ký hiệu là A(i,j,k).

### 1.5 Mối quan hệ giữa các đại lượng trên

Chúng ta có thể hình dung các mối quạn hệ này giống như việc bạn đang cầm một cây gậy:

>Scalar là chiều dài của cây gậy, nhưng bạn sẽ không biết cây gậy đang chỉ vào đâu.

>Vector không chỉ biết chiều dài của cây gậy, mà còn biết cây gậy chỉ vào mặt trước hay mặt sau.

>Tensor như một tập hợp của nhiều cây gậy không chỉ biết chiều dài của cây gậy, mà còn biết cây gậy đó đó chỉ về phía trước hay phía sau, và bao nhiêu cây gậy bị lệch lên / xuống và trái / phải.

### 2. Chuẩn norm của vecto và ma trận là gì?

Khi học về Machine Learning hoặc thiết kế các dạng mạng nơ ron chúng ta thường hay nhắc đến các khai niệm về **regular** là một hình thức để chuẩn hóa theo các norm như norm 1, norm 2, norm p. Câu hỏi này có thể kiểm tra được kiến thức của ứng viên về các khái niệm trong đại số tuyển tính.

### 2.1 Vector norm là gì?

![Recordit GIF](img/VectorNorm.PNG) 

![Recordit GIF](img/VectorNorm2.PNG) 

### 2.2 Norm của ma trận là gì? Giải thích rõ về chuẩn 1 và chuẩn 2 của ma trận.

![Recordit GIF](img/Normofmatrix.PNG)

### 3. Đạo hàm là gì ?

Khi các bạn tiếp cận với các phương pháp Machine Learning dựa trên sự tối ưu hóa một hàm mất mát nào đó thì chắc chắn chúng ta cần phải động đến đạo hàm. Vì vậy các khái niệm cơ bản về đạo hàm chúng ta cũng nên đề cập đến trong khi phỏng vấn.

![Recordit GIF](img/damham.PNG)

### 4. Trị riêng và vector riêng là gì? Nêu một vài tính chất của chúng.

![Recordit GIF](img/tririeng.PNG)

### 5. Xác suất là gì? Tại sao nên sử dụng xác suất trong machine learning?

Xác suất của một biến cố (event) là một đại lượng để đo khả năng xuất hiện của sự kiện đó. Sự xuất hiện của một sự kiện trong một thử nghiệm ngẫu nhiên là ngẫu nhiên, các thử nghiệm ngẫu nhiên có thể được lặp lại với số lượng lớn trong cùng điều kiện có xu hướng thể hiện các mô hình xác suất của sự kiện đó. Ngoài việc đối phó với những điều không chắc chắn, machine learning cũng cần phải đối phó với số lượng ngẫu nhiên. Sự không chắc chắn và ngẫu nhiên có thể đến từ nhiều nguồn, sử dụng lý thuyết xác suất để định lượng độ tin cậy của một mô hình machine learning. Lý thuyết xác suất đóng vai trò trung tâm trong học máy vì việc thiết kế các thuật toán học máy thường dựa vào các giả định xác suất về dữ liệu.

>Chẳng hạn, trong khóa học về Machine Learning của (Andrew Ng), có một giả thuyết Naive Bayes là một ví dụ về sự độc lập có điều kiện. Thuật toán huấn luyện được đưa ra dựa trên nội dung để xác định xem email có phải là thư rác hay không. Giả sử rằng điều kiện xác suất mà từ x xuất hiện trong tin nhắn là độc lập với từ y, bất kể tin nhắn đó có phải là thư rác hay không. Rõ ràng giả định này không phải là không mất tính tổng quát, bởi vì một số từ hầu như luôn luôn xuất hiện cùng một lúc. Tuy nhiên, kết quả cuối cùng là giả định đơn giản này ít ảnh hưởng đến kết quả và trong mọi trường hợp cho phép chúng ta có thể nhanh chóng xác định thư rác.

### 6. Biến ngẫu nhiên là gì? Nó khác gì so với biến đại số thông thường?

Trên thực tế, với một hàm số có thể cho các kết quả khác nhau trong một biến cố ngẫu nhiên (hay nói cách khác một hiện tượng không phải lúc nào cũng xuất hiện cùng một kết quả trong các điều kiện nhất định). Ví dụ, số lượng hành khách chờ tại trạm xe buýt tại một thời điểm nhất định, số lượng cuộc gọi mà tổng đài điện thoại nhận được tại một thời điểm nhất định, v.v., đều là ví dụ về các biến ngẫu nhiên. Sự khác biệt của biến ngẫu nhiên so với một biến đại số thông thường đó chính là xác suất. Khi xác suất giá trị của một biến không phải là 1 (nhỏ hơn 1) thì một biến đại số thông thường trở thành biến ngẫu nhiên. Nếu một biến ngẫu nhiên có xác suất xuất hiện giá trị là 1 thì nó trở thành biến đại số thông thường.

>Ví dụ: khi xác suất của biến x có giá trị 100 là 1, thì x = 100 được xác định và sẽ không thay đổi tức là nó là biến đại số thông thương. Khi xác suất của biến x có giá trị là 50 là 0,5 và xác suất để x có giá trị là 100 là 0.5. Tức là giá trị của biến sẽ thay đổi với các điều kiện khác nhau. Đó là một biến ngẫu nhiên.

### 7. Xác suất có điều kiện là gì? Cho ví dụ ?

![Recordit GIF](img/xacsuatcodk.PNG)

### 8. Khái niệm về kỳ vọng, phương sai và ý nghĩa của chúng?

### 8.1 Kì vọng - Expectation

Trong lý thuyết xác suất và thống kê, kỳ vọng toán học (hay trung bình, còn được gọi là kỳ vọng) là tổng xác suất của mỗi kết quả có thể có trong thử nghiệm nhân với kết quả. Nó phản ánh giá trị trung bình của các biến ngẫu nhiên. Kì vọng có một số tính chất sau:

![Recordit GIF](img/kyvong.PNG)

### 8.2 Phương sai - Variance

![Recordit GIF](img/phuongsai.PNG)

